use lambda_runtime::{run, service_fn, tracing, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use rand::distributions::{Distribution, Uniform};

fn roll_dice(_command: &str) -> Result<u32, Box<dyn std::error::Error>> {
    let sides = 6; // Assuming a six-sided dice
    if sides < 1 || sides > 6 {
        return Err("Number of sides must be between 1 and 6".into());
    }

    let mut rng = rand::thread_rng();
    let die = Uniform::from(1..=sides); // Create a range from 1 to sides
    Ok(die.sample(&mut rng)) // Generate a random number within the range
}



/// This is a made-up example. Requests come into the runtime as unicode
/// strings in json format, which can map to any structure that implements `serde::Deserialize`
/// The runtime pays no attention to the contents of the request payload.
#[derive(Deserialize)]
struct Request {
    command: String,
}

/// This is a made-up example of what a response structure may look like.
/// There is no restriction on what it can be. The runtime requires responses
/// to be serialized into json. The runtime pays no attention
/// to the contents of the response payload.
#[derive(Serialize)]
struct Response {
    req_id: String,
    msg: String,
}

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
/// - https://github.com/aws-samples/serverless-rust-demo/
async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Extract some useful info from the request
    let command = event.payload.command;

    let message = match roll_dice(&command.to_string()) {
        Ok(result) => format!("{}", result),
        Err(err) => format!("Error in rolling dice: {:?}", err),
    };    

    // Prepare the response
    let resp = Response {
        req_id: event.context.request_id,
        msg: message,
    };

    // Return `Response` (it will be serialized to JSON automatically by the runtime)
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await
}

