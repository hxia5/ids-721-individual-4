use aws_sdk_dynamodb::model::AttributeValue;
use aws_sdk_dynamodb::Client;
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

// Define the structure representing the response from db.rs
#[derive(Deserialize)]
struct Request {
    req_id: String,
    msg: String, // Adjusted to match the new request structure
}

/// This is a made-up example of what a response structure may look like.
/// There is no restriction on what it can be. The runtime requires responses
/// to be serialized into JSON. The runtime pays no attention
/// to the contents of the response payload.
#[derive(Serialize)]
struct Response {
    req_id: String,
    msg: String,
}

/// This is the main body for the function.
async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Extract some useful info from the request
    let req_id = event.payload.req_id.clone();
    let msg = event.payload.msg.clone();
    let config = aws_config::load_from_env().await;
    let client = Client::new(&config);

    // Send a request to fetch roll data from the database
    let resp = client.scan().table_name("diceroll").send().await?;

    // Initialize counters for each side of the dice
    let mut count_1 = 0;
    let mut count_2 = 0;
    let mut count_3 = 0;
    let mut count_4 = 0;
    let mut count_5 = 0;
    let mut count_6 = 0;

    // Iterate over each item in the response
    if let Some(items) = resp.items {
        for item in items {
            // Extract the roll_value from the HashMap
            let roll_value = match item.get("roll_value") {
                Some(attr) => match attr {
                    AttributeValue::S(s) => s.clone(),
                    _ => String::new(),
                },
                _ => String::new(),
            };

            // Increment the corresponding counter based on the roll value
            match roll_value.as_str() {
                "1" => count_1 += 1,
                "2" => count_2 += 1,
                "3" => count_3 += 1,
                "4" => count_4 += 1,
                "5" => count_5 += 1,
                "6" => count_6 += 1,
                _ => (),
            }
        }
    }


    // Prepare the response
    let response = Response {
        req_id,
        msg: format!(
            "Counts: 1: {}, 2: {}, 3: {}, 4: {}, 5: {}, 6: {}",
            count_1, count_2, count_3, count_4, count_5, count_6
        ),
    };

    // Return the response
    Ok(response)
}

/// Entry point of the application
#[tokio::main]
async fn main() -> Result<(), Error> {
    // Start the Lambda runtime
    run(service_fn(function_handler)).await
}
