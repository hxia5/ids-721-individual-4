use aws_sdk_dynamodb::model::AttributeValue;
use aws_sdk_dynamodb::Client;
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};

/// This is a made-up example. Requests come into the runtime as unicode
/// strings in JSON format, which can map to any structure that implements `serde::Deserialize`.
/// The runtime pays no attention to the contents of the request payload.
#[derive(Deserialize)]
struct Request {
    req_id: String,
    msg: String, // Adjusted to match the new request structure
}

// Define the structure to push to the DynamoDB table
#[derive(Debug, Serialize, Deserialize)]
pub struct DiceRoll {
    pub roll_id: String,
    pub roll_value: String,
}

// Define a structure for failure responses
#[derive(Debug, Serialize)]
struct FailureResponse {
    body: String,
}

impl std::error::Error for FailureResponse {}

impl std::fmt::Display for FailureResponse {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.body)
    }
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    msg: String,
}


/// This is the main body for the function.
async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Extract some useful info from the request
    let req_id = event.payload.req_id.clone();
    let msg = event.payload.msg.clone(); // Clone the message

    // Initialize DynamoDB client
    let config = aws_config::load_from_env().await;
    let client = Client::new(&config);

    // Create a DiceRoll object
    let dice_roll = DiceRoll {
        roll_id: req_id.clone(),
        roll_value: msg.clone(), // Use the cloned message as the roll value
    };

    // Convert DiceRoll fields to AttributeValues
    let roll_id_attribute = AttributeValue::S(dice_roll.roll_id.clone());
    let roll_value_attribute = AttributeValue::S(dice_roll.roll_value.clone());

    // Store data in the "diceroll" table in DynamoDB
    let _resp = client
        .put_item()
        .table_name("diceroll")
        .item("roll_id", roll_id_attribute)
        .item("roll_value", roll_value_attribute)
        .send()
        .await
        .map_err(|err| FailureResponse {
            body: err.to_string(),
        })?;

    // Prepare the response
    let resp = Response {
        req_id: event.context.request_id,
        msg: "Inserted into db".to_string(),
    };

    // Return `Response` (it will be serialized to JSON automatically by the runtime)
    Ok(resp)
}

/// Entry point of the application
#[tokio::main]
async fn main() -> Result<(), Error> {
    // Start the Lambda runtime
    run(service_fn(function_handler)).await
}



