# IDS 721 Individual 4


[![pipeline status](https://gitlab.com/hxia5/ids-721-individual-4/badges/main/pipeline.svg)](https://gitlab.com/hxia5/ids-721-individual-4/-/commits/main)

## Youtube Video Link
https://youtu.be/TABR9MVIijs

## AWS State Machine Execution/ Input$Output / Graph Screenshot

![alt text](execution.png)

![alt text](In&out.png)

![alt text](graph.png)

## Overview
* This is my repository of IDS 721 Individual Project 4 - Rust AWS Lambda and Step Functions

## Purpose
- Rust AWS Lambda function
- Step Functions workflow coordinating Lambdas
- Orchestrate data processing pipeline


## Key Steps

1. Install Rust and Cargo-Lambda, instructions can be found [here](https://www.cargo-lambda.info/guide/installation.html) and [here](https://www.rust-lang.org/tools/install).

2. Create a new Rust project using the following command:
```bash
cargo new ids-721-individual-4
```

3. Modify `main.rs`, `db.rs` and `count` to create three steps that rolls a dice first. then store it in the DynamoDB table, and finally count the number of times each number has been rolled.

3. Add necessary dependencies to the Cargo.toml file.

5. Run `cargo lambda watch` in one terminal, and `cargo lambda invoke --data-ascii "{ \"command\": \"roll\" }` in another terminal to test the function.

6. Sign up for an AWS account and create a new IAM user with programmatic access and attach the `lambdafull_access` and `iamfullaccess` policy.

7. Click on the user, under the `Security credentials` tab, click on `Create access key`.

8. Copy the `access key ID` and `secret access key`, select a region under the tab at the top right corner. Create a `.env` file in the root directory of the project and add the `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION`.

9. Put `.env` in `.gitignore` to avoid exposing sensitive information.

10. Build the project using `cargo lambda build` and deploy the project using `cargo lambda deploy`. However, there's something wrong with my laptop, since my code can run, I use `Makefile` to deploy the project.

11. Create a `.yml` file for the pipeline and enable auto build, test, and deploy of your lambda function every time you push.

12. After it paased, search for `Lambda` in the AWS Management Console, after redirecting to the Lambda service, make sure the region is the same as the one in the `.env` file.

13. Your function should be deployed and ready to be tested. Click on the step2 anf step3 function and go to the `Configuration` tab, and click on `Permissions` then click the `Role name` to add the `AmazonDynamoDBFullAccess` policy.

14. Go to DynamoDB and create a table named `diceroll` with the primary key as `roll_id`.

15. Create a state machine in the Step Functions service, and add the three lambda functions as steps. Remember to select the one with `LATEST` label for each step.

16. Start execution and check the output.

## Reference:
1. https://www.youtube.com/watch?v=qoVAEflgcZ4